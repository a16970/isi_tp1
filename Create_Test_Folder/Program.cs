﻿using System;
using System.IO;
using System.Linq;

namespace Create_Test_Folder
{
    class Program
    {
        static void Main(string[] args)
        {
            var movies = Directory.GetDirectories(@"E:\Movies\")
                .Select(p => new {path = p, name = Path.GetFileName(p)})
                .ToArray();
            Directory.Delete(@"D:\Temp\Movies\", true);
            foreach (var movie in movies)
            {
                Directory.CreateDirectory(@"D:\Temp\Movies\" + movie.name);
            }

        }
    }
}
