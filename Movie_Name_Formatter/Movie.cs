﻿using System.ComponentModel;

namespace Movie_Name_Formatter
{
    public class Movie
    {

        [DefaultValue("")]
        public string Path { get; set; }

        [DefaultValue("")]
        public string OriginalName { get; set; }

        [DefaultValue("")]
        public string NewName { get; set; }

        [DefaultValue("")]
        public string Year { get; set; }

        [DefaultValue("")]
        public string Quality { get; set; }

        /// <summary>
        /// Class movie constructor (Helps to initialize the object in a controlled way)
        /// </summary>
        /// <param name="path"> Path where the movie folder is located </param>
        /// <param name="originalName"> Original name of the movie without the path </param>
        public Movie(string path, string originalName)
        { 
            Path = path;
            OriginalName = originalName;
        }

        /// <summary>
        /// Groups the movie information in a formatted form
        /// </summary>
        /// <returns> Returns the formatted string with available movie information </returns>
        public override string ToString()
        {
            return Year != "" ? (Quality != "" ? NewName + " - " + Year + " - " + Quality : NewName + " - " + Year) : (Quality != "" ? NewName + " - " + Quality : NewName);
        }
    }
}