﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Movie_Name_Formatter
{
    class Program
    {
        static void Main(string[] args)
        { 
            // Input folder
            const string FolderPath = @"D:\Temp\Movies\";

            // Regular expressions
            const string MovieNameYearRegex =           // Gets the movie name and year
                "(.+?)[.(\\s\t-]*(?:(19\\d{2}|20(?:0|1|2)\\d).*|(?:(?=bluray|\\d+p|brrip|\\dk|internal)..*)?$)";
            const string MovieQualityRegex =            // Gets the movie quality
                ".+?(?:(\\d{4}p|720p|\\dk|3D).*)?$";

            // Log Report
            var log = "";
            
            // Chronometer
            var watch = Stopwatch.StartNew();

            // List of all the movies to work with
            var movies = Directory.GetDirectories(FolderPath)
                .Select(p => new Movie( p, Path.GetFileName(p)))
                .ToList();

            // Already tried Parallel.foreach but it takes longer to execute than the sequential
            foreach (var movie in movies)
            {
                // Log Report for each movie
                //  useful in a parallel execution ( helps maintain report integrity )
                var sublog = "";

                try
                {
                    sublog += "Name to read: " + movie.OriginalName + "\n";

                    movie.NewName = Regex.Replace(movie.OriginalName, @"(\s+|\.)", " ");            // Changes multiple spaces and dots in the movie name to a single space

                    var match = Regex.Match(movie.NewName, MovieNameYearRegex, RegexOptions.IgnoreCase);      // Match the regular expression getting a group with the movie name and other with the year

                    movie.NewName = match.Groups[1].Value;
                    movie.Year = match.Groups[2].Value;

                    match = Regex.Match(movie.OriginalName, MovieQualityRegex, RegexOptions.IgnoreCase);      // Match the regular expression getting a group with the movie quality

                    movie.Quality = match.Groups[1].Value;

                    sublog += "Formatted Name: " + movie.ToString() + "\n";

                    JsonMovieInfoWrite(movie);                                                                            // Saves the movie information in a json file

                    if (movie.Path != (FolderPath + movie.ToString()))                                                    // Checks if the folder name was already formatted correctly
                    {
                        Directory.Move(movie.Path, FolderPath + movie.ToString());
                        sublog += "\tDirectory " + movie.ToString() + " changed!\n";
                    }
                    else
                    {
                        sublog += "\tDirectory " + movie.ToString() + " unchanged!\n";
                    }
                }
                catch (Exception e)
                {
                    sublog += "\tException: " + e.Message + "\n";                                                         // In the event of an exception, this is saved in the execution report
                }

                log += sublog;      // Saves sublog at the main log report
            }

            watch.Stop();

            log += "\nElapsed Time: " + watch.ElapsedMilliseconds + "ms\n";

            File.WriteAllText("./logfile.log", log);        // Writes the Log file

            Email_send();                                               // Sending an email with the execution report

            Console.WriteLine(log);
        }

        /// <summary>
        /// Sends an email with the log file
        /// </summary>
        private static void Email_send()
        {
            var mail = new MailMessage();
            var smtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("a16970.ipca@gmail.com");
            mail.To.Add("a16970@alunos.ipca.pt");
            mail.To.Add("a17016@alunos.ipca.pt");
            mail.Subject = "Movie Name Formatter";
            mail.Body = "Success!";

            var attachment = new Attachment(@"./logfile.log");
            mail.Attachments.Add(attachment);

            smtpServer.Port = 587;
            smtpServer.Credentials = new System.Net.NetworkCredential("a16970.ipca@gmail.com", "IPCA.a16970");      // Email created especially for academic work
            smtpServer.EnableSsl = true;

            smtpServer.Send(mail);

        }

        /// <summary>
        /// Writes movie info into json file inside movie folder
        /// </summary>
        /// <param name="movie">movie to save</param>
        private static void JsonMovieInfoWrite(Movie movie)
        {
            using (var file = File.CreateText(movie.Path + @"\" + movie.NewName + ".json"))
            {
                var serializer = new JsonSerializer
                    {NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore};
                serializer.Serialize(file, movie);
            }
        }
    }
}
